# Etherum App
An app which helps in transfering of  Eth assets.

## Name
Ethereum transaction App using BLockchain on Web 3.0.

## Description
Technologies used in implementation of this Application are as follows:
### Web 3.0:
The web 3.0 definition varies depending on who you ask, but in general, it’s considered the third generation of the internet. Web 3.0 is also called the semantic web or the spatial web. If you break the evolution of the internet into segments with the first being simple web pages and the second being apps, social media, and mass adoption, the emerging blockchain-based web would be internet 3.0. Essentially, it’s a user-friendly, more secure, more private and better connected internet.
Web 3.0 utilizes blockchain technology to create true decentralization. Not only will this be the latest step in the technology that runs the internet, but this will also be the first proper step towards the metaverse. A fair metaverse, with digital data ownership through NFTs, is only possible on web 3.0.
In this third generation of the internet, devices will be connected in a decentralized network rather than relying on server-based databases. We won’t need to keep all our data on centralized servers, rather we will own it ourselves independently. We get to choose what we do with it.

### Blockchain:
Blockchain is a shared, immutable ledger that facilitates the process of recording transactions and tracking assets in a business network. An asset can be tangible (a house, car, cash, land) or intangible (intellectual property, patents, copyrights, branding). Virtually anything of value can be tracked and traded on a blockchain network,reducing risk and cutting costs for all involved.

### Solidity:
Solidity is a contract-oriented, high-level programming language for implementing smart contracts. Solidity is highly influenced by C++, Python and JavaScript and has been designed to target the Ethereum Virtual Machine (EVM).

### MetaMask:
MetaMask is a Crypto Wallet and Your Gateway to Web3.Buy, store and send tokens globally.Explore blockchain applications at lightening speed & choose what to share and what to keep private.

## Workflow of Ethereum transaction:
![how-does-a-blockchain-work](https://ik.imagekit.io/rkg8ugw0kq/assets/images/blog/1562846735/how-does-a-blockchain-work.jpg)

## Visuals
* HomePage:
![Main Page](https://drive.google.com/file/d/13DNIS40bdrSCy7zerm-Q98DNx9WTSIOy/view?usp=sharing)
* Services:
![Services](https://drive.google.com/file/d/1-C9T2LfOBKcss2dGPzo6GZu6ngI9h2Ue/view?usp=sharing)
* Record on App:
![Transactions](https://drive.google.com/file/d/1QBGj8q_iy4gDyqtVquIdY8m25YTGMgTa/view?usp=sharing)
* Record on Remote Server:
![Etherscan](https://drive.google.com/file/d/12jiiQcsaz3zFtKUc_AMnpKll19Zoz8mt/view?usp=sharing)

## Installation
* Visual Studio Code
* NodeJs
* Solidity Extension
* MetaMask

## Usage
This Application can be used to send Ethers globally through a secured Blockchain passage.

## Roadmap
I am further ahead with an idea of implementation of Crypto mining in the application after which it will be deployed on Web.

## Authors and acknowledgment
Aarush Kumar 

## License
Released under Apache2.0 community license.

## Project status
Crypto-mining is under coding phase.

## Demo:
Working demo of Application can be accessed at:[Youtube](https://www.youtube.com/watch?v=pZ0uzUHCvJk&t=3s)

## WebApp can be accessed at:
[ethereum-transfer](https://aarush-ethereum.netlify.app/)

Thankyou!!!
